﻿#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "animal voice";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Goat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Ba-a-a\n";
    }
};


class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow\n";
    }
};



int main()
{
    const int n = 4;
    Animal* animals[n];
    Dog dog1, dog2;
    Cat cat;
    Goat goat;

    animals[0] = &dog1;
    animals[1] = &cat;
    animals[2] = &dog2;
    animals[3] = &goat;

    for (int i = 0; i < n; i++)
    {
        animals[i]->Voice();
    }

}